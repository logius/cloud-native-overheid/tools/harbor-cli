module gitlab.com/logius/cloud-native-overheid/tools/harbor-cli

go 1.22

toolchain go1.22.2

require (
	code.cloudfoundry.org/bytefmt v0.0.0-20211005130812-5bb3c17173e5
	github.com/docker/cli v20.10.10+incompatible
	github.com/google/go-containerregistry v0.7.0
	github.com/hashicorp/go-version v1.6.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.8.0
	github.com/xanzy/go-gitlab v0.101.0
	gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli v0.0.0-20240409094811-e7e7cf451086
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/containerd/stargz-snapshotter/estargz v0.10.0 // indirect
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v20.10.10+incompatible // indirect
	github.com/docker/docker-credential-helpers v0.6.4 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.2 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/opencontainers/image-spec v1.0.2-0.20210730191737-8e42a01fb1b7 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/vbatts/tar-split v0.11.2 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/oauth2 v0.6.0 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/time v0.3.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.29.1 // indirect
)

// replace (
// 	gitlab.com/logius/cloud-native-overheid/tools/environment-cli => ../environment-cli
// 	gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli => ../gitlab-cli
//  gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli => ../keycloak-cli
// 	gitlab.com/logius/cloud-native-overheid/tools/kibana-cli => ../kibana-cli
// )
