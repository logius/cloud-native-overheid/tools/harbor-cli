package project

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
	"gitlab.com/logius/cloud-native-overheid/tools/harbor-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/harbor-cli/harbor"
)

type flags struct {
	config                   *string
	dockerAuthConfig         *string
	altHarborUrls            *[]string
	harborURL                *string
	harborUser               *string
	harborPassword           *string
	gitLabURL                *string
	gitlabURLPeer            *string
	gitLabVarPrefix          *string
	gitLabVarSuffix          *[]string
	overwriteRetentionPolicy *bool
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := configureProject(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "configure-customer",
		Short: "Configure project in Harbor",
		Long:  "This command configures a project for a tenant in Harbor.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.harborURL = cmd.Flags().String("url", "", "URL of Harbor")
	flags.gitLabURL = cmd.Flags().String("gitlab-url", "", "URL of Gitlab")
	flags.gitlabURLPeer = cmd.Flags().String("gitlab-url-peer", "", "URL of GitLab DR site")
	flags.gitLabVarPrefix = cmd.Flags().String("gitlab-var-prefix", "HARBOR_", "Gitlab variables prefix")
	flags.gitLabVarSuffix = cmd.Flags().StringArray("gitlab-var-suffix", nil, "Gitlab variables suffix")
	flags.harborUser = cmd.Flags().String("harbor-user", os.Getenv("HARBOR_USERNAME"), "Harbor username")
	flags.harborPassword = cmd.Flags().String("harbor-password", os.Getenv("HARBOR_PASSWORD"), "Harbor password")
	flags.dockerAuthConfig = cmd.Flags().String("docker-auth-config", "DOCKER_AUTH_CONFIG.json", "DOCKER_AUTH_CONFIG containing the harbor configuration")
	flags.altHarborUrls = cmd.Flags().StringArray("alternate-harbor-url", nil, "List of Harbor hostnames that use the same authentication token in DOCKER_AUTH_CONFIG")
	flags.overwriteRetentionPolicy = cmd.Flags().Bool("overwrite-retention-policies", false, "Always (re)apply the supplied retention policy file")

	_ = cmd.MarkFlagRequired("config")
	_ = cmd.MarkFlagRequired("url")
	_ = cmd.MarkFlagRequired("gitlab-url")

	return cmd
}

func configureProject(cmd *cobra.Command, flags *flags) error {

	harborClient := &harbor.Client{
		Username: *flags.harborUser,
		Password: *flags.harborPassword,
		FQDN:     *flags.harborURL,
	}

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	// create registries
	harborRegistries := make(map[string]int)
	for _, registry := range customerConfig.Harbor.Registries {
		registryID, err := harborClient.EnsureRegistryExists(registry)
		if err != nil {
			return err
		}
		harborRegistries[registry.Name] = registryID
		log.Printf("Harbor registry registered, %s\n", registry.Name)
	}

	// create projects
	harborProjects := make(map[string]int)
	for _, project := range customerConfig.Harbor.Projects {
		log.Printf("Running configuration for project %s", project.Name)
		projectID, err := harborClient.EnsureProjectExists(project, *flags.overwriteRetentionPolicy)
		if err != nil {
			return err
		}
		harborProjects[project.Name] = projectID
		log.Printf("Harbor project registered, %s\n", project.Name)
	}

	// Manage Robot Accounts + Gitlab variables
	gitLabClient := gitlabclient.NewGitLabClient(*flags.gitLabURL)

	var gitlabClientPeer *gitlab.Client
	if *flags.gitlabURLPeer != "" {
		gitlabClientPeer, _ = gitlab.NewClient(os.Getenv("GITLAB_ACCESS_TOKEN"), gitlab.WithBaseURL(fmt.Sprintf("%s/api/v4", *flags.gitlabURLPeer)))

		_, _, err := gitlabClientPeer.Version.GetVersion()
		if err != nil {
			gitlabClientPeer = nil
			log.Printf("Ignoring error updating gitlab variables for the DR site. err= %v", err)
		}
	}

	systemRobotAccount := customerConfig.Harbor.OneRobotAccPerCustomer
	// if OneRobotAccountPerCustomer is true create System Robot accounts else create Project Robot accounts
	if systemRobotAccount {
		customerName := customerConfig.Name
		groupVars, err := harborClient.ManageSystemRobotAccount(harborProjects, customerName, "system", *flags.gitLabVarPrefix, *flags.dockerAuthConfig, *flags.altHarborUrls)
		if err != nil {
			return err
		}

		groupVars = harbor.CreateSuffixedGitlabGroupVars(groupVars, *flags.gitLabVarSuffix)

		for _, gitlabGroup := range customerConfig.GitLab.Groups {
			_ = gitlabclient.UpdateVarsInGitLabGroup(gitLabClient, gitlabGroup.Name, groupVars)

			if gitlabClientPeer != nil {
				_ = gitlabclient.UpdateVarsInGitLabGroup(gitlabClientPeer, gitlabGroup.Name, groupVars)
			}
		}
	} else {
		for harborProjectName, projectID := range harborProjects {
			defaultProject := false
			if customerConfig.Harbor.Projects[0].Name == harborProjectName {
				defaultProject = true
			}
			// create DOCKER_AUTH_CONFIG when project is not public
			createDockerAuthConfig := !isProjectPublic(harborProjectName, customerConfig.Harbor.Projects)
			groupVars, err := harborClient.ManageProjectRobotAccounts(projectID, harborProjectName, defaultProject, createDockerAuthConfig)
			if err != nil {
				return err
			}

			groupVars = harbor.CreateSuffixedGitlabGroupVars(groupVars, *flags.gitLabVarSuffix)

			for _, gitlabGroup := range customerConfig.GitLab.Groups {
				err = gitlabclient.UpdateVarsInGitLabGroup(gitLabClient, gitlabGroup.Name, groupVars)
				if err != nil {
					return err
				}

				if gitlabClientPeer != nil {
					err = gitlabclient.UpdateVarsInGitLabGroup(gitlabClientPeer, gitlabGroup.Name, groupVars)
					if err != nil {
						log.Printf("Ignoring error updating gitlab variables for the DR site. err= %v", err)
					}
				}
			}
		}
	}
	return nil
}

func isProjectPublic(projectName string, configProjects []config.Project) bool {
	for i := range configProjects {
		if configProjects[i].Name == projectName {
			return configProjects[i].IsPublic()
		}
	}
	return false
}
