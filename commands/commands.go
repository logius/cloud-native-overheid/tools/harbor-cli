package commands

import (
	"github.com/spf13/cobra"
	copyimages "gitlab.com/logius/cloud-native-overheid/tools/harbor-cli/copy-images"
	"gitlab.com/logius/cloud-native-overheid/tools/harbor-cli/project"
	"gitlab.com/logius/cloud-native-overheid/tools/harbor-cli/robot"
)

// AddMainCommand adds the main command for the current module
func AddMainCommand(rootCmd *cobra.Command) {
	cmd := &cobra.Command{
		Use:   "harbor",
		Short: "OPS tools for Harbor registry",
	}
	rootCmd.AddCommand(cmd)

	AddSubCommands(cmd)

}

// AddSubCommands adds subcommands
func AddSubCommands(cmd *cobra.Command) {
	cmd.AddCommand(project.NewCommand())
	cmd.AddCommand(robot.NewCommand())
	cmd.AddCommand(copyimages.NewCommand())
	cmd.AddCommand(copyimages.NewPurgeCommand())
}
