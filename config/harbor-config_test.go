package config

import (
	"testing"
)

var data = `
customer:
  name: tc
  support: true

  groups:
  - name: tc-developer
  - name: tc-maintainer

  harbor:
    projects:
    - name: tc-prj-test-1
      roles:
      - name: guest
        groups:
        - tc-developer
        - tc-maintainer
    - name: tc-prj-test-2
      roles:
      - name: maintainer
        groups:
        - tc-developer
        - tc-maintainer
  `

func TestParse(t *testing.T) {

	got := NewHarborConfig(data)

	if len(got.Projects) == 0 {
		t.Errorf("No projects found")
	} else {
		if got.Projects[0].Name != "tc-prj-test-1" {
			t.Errorf("got.Projects[0].Name = %v, but want tc-prj-test", got.Projects[0].Name)
		}
		if got.Projects[0].Rolebindings == nil {
			t.Errorf("got.Projects[0].Roles == nil")
		} else {
			if len(got.Projects[0].Rolebindings) < 1 {
				t.Errorf("len(got.Projects[0].Rolebindings) < 1")
			}
		}
	}
}
