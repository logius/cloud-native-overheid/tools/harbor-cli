package config

import (
	"io/ioutil"
	"log"
	"strconv"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

// Rolebinding defines the binding between rol and zero or more groups
type Rolebinding struct {
	Name   string
	Groups []string `yaml:"groups,omitempty"`
}

// Registry for proxy cache endpoint
type Registry struct {
	Name string
	Type string
	Url  string
}

// Project defines the configuration of the project
type Project struct {
	Name                   string
	Public                 *bool
	RegistryName           string        `yaml:"registry_name,omitempty"`
	Rolebindings           []Rolebinding `yaml:"roles"`
	RetentionFile          string        `yaml:"retention_file,omitempty"`
	PreventVulnFromRunning *bool         `yaml:"prevent_vuln_from_running"`
	StorageQuota           string
}

// If Public is nil, returns true, otherwise the value
func (p Project) IsPublic() bool {
	return p.Public == nil || *p.Public
}

// If PreventVulnFromRunning is empty, returns string "true", otherwise the value
func (p Project) PreventingVulnFromRunning() string {
	if p.PreventVulnFromRunning == nil {
		return "true"
	} else {
		return strconv.FormatBool(*p.PreventVulnFromRunning)
	}
}

// Harbor configuration
type Harbor struct {
	Registries             []Registry
	Projects               []Project
	OneRobotAccPerCustomer bool `yaml:"one_robot_account_per_customer"`
}

type GitLabGroup struct {
	Name string `validate:"required"`
}

// GitLab configuration
type GitLab struct {
	Groups []GitLabGroup `validate:"required,dive"`
}

type Customer struct {
	Name   string
	GitLab GitLab `validate:"required"`
	Harbor Harbor `validate:"required"`
}

type Config struct {
	Customer Customer `validate:"required"`
}

// LoadConfigfile loads the customer configuration from file
func LoadConfigfile(configFile string) (*Customer, error) {
	yamlFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, errors.Errorf("Error reading YAML file: %s\n", err)
	}
	return NewConfig(string(yamlFile))
}

// NewConfig gets the customer configuration
func NewConfig(configData string) (*Customer, error) {

	var cfg Config
	err := yaml.Unmarshal([]byte(configData), &cfg)
	if err != nil {
		return nil, errors.Errorf("Could not parse the config err = %v", err)
	}

	return &cfg.Customer, nil
}

// NewHarborConfig gets the project configuration
func NewHarborConfig(configData string) *Harbor {
	type T struct {
		Customer struct {
			GitLab GitLab
			Harbor Harbor
		}
	}
	var cfg T

	err := yaml.Unmarshal([]byte(configData), &cfg)
	if err != nil {
		log.Fatalf("Could not parse the config err = %v", err)
	}

	return &cfg.Customer.Harbor
}
