package robot

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
	"gitlab.com/logius/cloud-native-overheid/tools/harbor-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/harbor-cli/harbor"
)

type flags struct {
	config              *string
	harborURL           *string
	harborUser          *string
	harborPassword      *string
	gitLabURL           *string
	gitLabVarPrefix     *string
	gitLabVarSuffix     *[]string
	dockerAuthConfig    *string
	altHarborUrls       *[]string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := configureRobot(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "configure-robot-account",
		Short: "Configure robot account in Harbor",
		Long:  "This command configures a robot account for a tenant in Harbor.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.harborURL = cmd.Flags().String("url", "", "URL of Harbor")
	flags.gitLabURL = cmd.Flags().String("gitlab-url", "", "URL of Gitlab")
	flags.gitLabVarPrefix = cmd.Flags().String("gitlab-var-prefix", "HARBOR_", "Gitlab variables prefix")
	flags.gitLabVarSuffix = cmd.Flags().StringArray("gitlab-var-suffix", nil, "Gitlab variables suffix")
	flags.harborUser = cmd.Flags().String("harbor-user", os.Getenv("HARBOR_USERNAME"), "Harbor username")
	flags.harborPassword = cmd.Flags().String("harbor-password", os.Getenv("HARBOR_PASSWORD"), "Harbor password")
	flags.dockerAuthConfig = cmd.Flags().String("docker-auth-config", "DOCKER_AUTH_CONFIG.json", "DOCKER_AUTH_CONFIG containing the Harbor authentication tokens and configuration")
	flags.altHarborUrls = cmd.Flags().StringArray("alternate-harbor-url", nil, "List of Harbor urls that use the same authentication token in DOCKER_AUTH_CONFIG")

	_ = cmd.MarkFlagRequired("config")
	_ = cmd.MarkFlagRequired("url")
	_ = cmd.MarkFlagRequired("gitlab-url")

	return cmd
}

func configureRobot(cmd *cobra.Command, flags *flags) error {

	harborClient := &harbor.Client{
		Username: *flags.harborUser,
		Password: *flags.harborPassword,
		FQDN:     *flags.harborURL,
	}

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	//Get project ID's
	harborProjects := make(map[string]int)
	for _, project := range customerConfig.Harbor.Projects {
		projectID, err := harborClient.GetProjectID(project.Name)
		if err != nil {
			return err
		}
		harborProjects[project.Name] = projectID
		log.Printf("Harbor project found, %s\n", project.Name)
	}

	// Manage Robot Accounts + Gitlab variables
	gitLabClient := gitlabclient.NewGitLabClient(*flags.gitLabURL)
	customerName := customerConfig.Name
	groupVars, err := harborClient.ManageSystemRobotAccount(harborProjects, customerName, "system", *flags.gitLabVarPrefix, *flags.dockerAuthConfig, *flags.altHarborUrls)
	if err != nil {
		return err
	}

	groupVars = harbor.CreateSuffixedGitlabGroupVars(groupVars, *flags.gitLabVarSuffix)
	for _, gitlabGroup := range customerConfig.GitLab.Groups {
		_ = gitlabclient.UpdateVarsInGitLabGroup(gitLabClient, gitlabGroup.Name, groupVars)
	}

	return nil
}
