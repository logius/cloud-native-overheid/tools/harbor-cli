# harbor-cli

## Commands
For the harbor-cli you can make use of two possible subcommands to trigger full project(s) creations including robot-accounts, or just create a system-level robot-account to access existing projects. The latter option makes it also possible to configure a robot account that has access to projects outside of customer's scope.

Subcommands:
1. configure-customer
2. configure-robot-account

### Configure-customer 
#### Default behavior
Based on the customer config Harbor projects are created, project authorization is managed. Robot accounts are created on a per project basis.

The harbor robot credentials will be stored in the gitlab customer group (`customer.gitlab.groups`) variables with names:
- HARBOR_ROBOT_NAME
- HARBOR_ROBOT_SECRET
- <harborProjectNameUpper>_HARBOR_ROBOT_NAME (per project)
- <harborProjectNameUpper>_HARBOR_ROBOT_SECRET (per project)
- DOCKER_AUTH_CONFIG

#### Feature create one robot account per customer
This feature creates a robot account on "system" level. The feature defaults to create a robot account on "project" level based on the harbor projects defined in the harbor section. The example below shows the configuration and how to create a robot account on "system" level instead.
[examples/customer.yaml]

The harbor robot credentials will be stored in the gitlab customer group variables with name:
- HARBOR_ROBOT_NAME
- HARBOR_ROBOT_SECRET
- DOCKER_AUTH_CONFIG

#### Run local

Build image local:
```sh
./build/build.sh
```

Collect credentials for Harbor and Gitlab
Use the local build docker image to apply the customer.yaml:
```sh
HARBOR_URL=
HARBOR_USERNAME=
HARBOR_PASSWORD=

GITLAB_URL=
GITLAB_ACCESS_TOKEN=

LOCAL_CUSTOMER_CONFIG_FILE=$(pwd)/examples/customer.yaml
DEST_CUSTOMER_CONFIG_FILE=/customer.yaml

HARBOR_CLI_IMAGE=registry.gitlab.com/logius/cloud-native-overheid/tools/harbor-cli:local

docker run -v $LOCAL_CUSTOMER_CONFIG_FILE:$DEST_CUSTOMER_CONFIG_FILE \
  -e HARBOR_USERNAME=$HARBOR_USERNAME \
  -e HARBOR_PASSWORD=$HARBOR_PASSWORD \
  -e GITLAB_ACCESS_TOKEN=$GITLAB_ACCESS_TOKEN \
  $HARBOR_CLI_IMAGE \
  configure-customer \
  --url=${HARBOR_URL} \
  --config=$DEST_CUSTOMER_CONFIG_FILE \
  --gitlab-url=${GITLAB_URL}
```

### Configure-robot-account
#### Default behavior
Based on the config provided the system-level robot account is given access to multiple projects.

The harbor robot credentials will be stored in the gitlab customer group (`customer.gitlab.groups`) variables with names:
- HARBOR_ROBOT_NAME
- HARBOR_ROBOT_SECRET
- DOCKER_AUTH_CONFIG

#### System-level robot account
This feature creates a robot account on "system" level. The example below shows the configuration and how to create a robot account on "system" level.
[examples/robot-config.yaml]

The harbor robot credentials will be stored in the gitlab customer group variables with name:
- HARBOR_ROBOT_NAME
- HARBOR_ROBOT_SECRET
- DOCKER_AUTH_CONFIG

#### Run local

Build image local:
```sh
./build/build.sh
```

Collect credentials for Harbor and Gitlab
Use the local build docker image to apply the customer.yaml:
```sh
HARBOR_URL=
HARBOR_USERNAME=
HARBOR_PASSWORD=

GITLAB_URL=
GITLAB_ACCESS_TOKEN=
LOCAL_ROBOT_CONFIG_FILE=$(pwd)/examples/customer.yaml
DEST_ROBOT_CONFIG_FILE=/robot-config.yaml
LOCAL_ROBOT_CONFIG_FILE=$(pwd)/examples/robot-config.yaml
HARBOR_CLI_IMAGE=registry.gitlab.com/logius/cloud-native-overheid/tools/harbor-cli:local

docker run -v ${LOCAL_CUSTOMER_CONFIG_FILE}:${DEST_CUSTOMER_CONFIG_FILE} \
  -e HARBOR_USERNAME=${HARBOR_USERNAME} \
  -e HARBOR_PASSWORD=${HARBOR_PASSWORD} \
  -e GITLAB_ACCESS_TOKEN=${GITLAB_ACCESS_TOKEN} \
  $HARBOR_CLI_IMAGE \
  configure-robot-account \
  --url=${HARBOR_URL} \
  --config=${DEST_CUSTOMER_CONFIG_FILE} \
  --gitlab-url=${GITLAB_URL}
```
