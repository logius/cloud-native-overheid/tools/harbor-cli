package copyimages

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/harbor-cli/harbor"
)

type purgeFlags struct {
	config    *string
	harborURL *string
}

// NewCommand creates a new command
func NewPurgeCommand() *cobra.Command {

	purgeFlags := purgeFlags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := purgeImages(cmd, &purgeFlags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "purge-images",
		Short: "Purge images from Harbor project",
		Long:  "This command cleans up repositories that are not included in the list of images.",
	}

	purgeFlags.config = cmd.Flags().String("config", "", "Path to file with a list of images ")
	purgeFlags.harborURL = cmd.Flags().String("harbor-url", "", "URL of Harbor")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("harbor-url")

	return cmd
}

func purgeImages(cmd *cobra.Command, flags *purgeFlags) error {

	namespaceImages, err := LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	harborClient := &harbor.Client{
		Username: os.Getenv("HARBOR_USERNAME"),
		Password: os.Getenv("HARBOR_PASSWORD"),
		FQDN:     *flags.harborURL,
	}

	projectName := "platform-images"
	repositories, err := harborClient.GetRepositories(projectName)
	if err != nil {
		return err
	}

	for _, repository := range repositories {

		artifacts, err := harborClient.GetArtifacts(projectName, repository.Name)
		if err != nil {
			return err
		}
		for _, artifact := range artifacts {
			for _, tag := range artifact.Tags {
				taggedArtifact := repository.Name + ":" + tag.Name
				if !exists(namespaceImages, taggedArtifact) {
					log.Printf("Remove %q", taggedArtifact)
				}
			}
		}

	}
	return nil
}

func exists(namespaces map[string][]string, repository string) bool {
	for _, images := range namespaces {

		for _, sourceImage := range images {

			destinationImage := sourceImage
			if strings.Contains(sourceImage, "/") {
				parts := strings.Split(sourceImage, "/")
				if strings.Contains(parts[0], ".") {
					destinationImage = strings.Join(parts[1:], "/")
				}
			}
			fullDestinationImage := fmt.Sprintf("%s/%s", "platform-images", destinationImage)
			if fullDestinationImage == repository {
				log.Printf("Keep %q", fullDestinationImage)
				return true
			}
		}
	}
	return false
}
