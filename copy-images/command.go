package copyimages

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"
	"sync"

	"github.com/docker/cli/cli/config"
	"github.com/docker/cli/cli/config/types"
	"github.com/google/go-containerregistry/pkg/authn"
	"github.com/google/go-containerregistry/pkg/crane"
	"github.com/google/go-containerregistry/pkg/name"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v3"
)

type flags struct {
	config         *string
	harborRegistry *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := copy(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "copy-images",
		Short: "Replicate a list of images",
		Long:  "This command replicates a list of images from a source registry to a target registry.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to file with a list of images ")
	flags.harborRegistry = cmd.Flags().String("harbor-registry", "", "FQDN of Harbor")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("harbor-registry")

	return cmd
}

var wg, wg2 sync.WaitGroup

type Job struct {
	Namespace string
	Source    string
	Target    string
	Error     error
}

func copy(cmd *cobra.Command, flags *flags) error {

	namespaceImages, err := LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	loginOptions := loginOptions{
		serverAddress: *flags.harborRegistry,
		user:          os.Getenv("HARBOR_USERNAME"),
		password:      os.Getenv("HARBOR_PASSWORD"),
	}
	if err := login(loginOptions); err != nil {
		return err
	}

	namespaces := make([]string, 0, len(namespaceImages))
	for k := range namespaceImages {
		namespaces = append(namespaces, k)
	}
	sort.Strings(namespaces)

	jobs := make(chan *Job, 100)    // Buffered channel
	results := make(chan *Job, 100) // Buffered channel

	// Start workers:
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go replicate(i, jobs, results)
	}

	for _, namespace := range namespaces {

		for _, sourceImage := range namespaceImages[namespace] {

			destinationImage := sourceImage
			if strings.Contains(sourceImage, "/") {
				parts := strings.Split(sourceImage, "/")
				if strings.Contains(parts[0], ".") {
					destinationImage = strings.Join(parts[1:], "/")
				}
			}
			fullDestinationImage := fmt.Sprintf("%s/%s/%s", *flags.harborRegistry, "platform-images", destinationImage)

			jobs <- &Job{Namespace: namespace, Source: sourceImage, Target: fullDestinationImage}
		}
	}

	close(jobs)

	wg.Wait() // Wait all workers to finish processing jobs

	// All jobs are processed, no more values will be sent on results:
	close(results)

	wg2.Wait() // Wait all results

	for job := range results {
		if job.Error != nil {
			return job.Error
		}
	}
	return nil
}

func replicate(id int, jobs <-chan *Job, results chan<- *Job) {
	defer wg.Done()
	for job := range jobs {
		log.Printf("%v: %v >> %v", job.Namespace, job.Source, job.Target)

		results <- job
		for i := 1; ; i++ {
			if err := crane.Copy(job.Source, job.Target); err != nil {
				if i == 5 {
					log.Printf("%v", err)
					job.Error = err
				} else {
					continue
				}
			}
			break
		}
	}
}

func LoadConfigfile(configFile string) (map[string][]string, error) {
	configData, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, errors.Errorf("Error reading YAML file: %s\n", err)
	}
	var namespaces map[string][]string
	if err := yaml.Unmarshal(configData, &namespaces); err != nil {
		return nil, errors.Errorf("Could not parse the config err = %v", err)
	}

	return namespaces, nil
}

type loginOptions struct {
	serverAddress string
	user          string
	password      string
	passwordStdin bool
}

// login method is copied from cmd/crane/cmd/auth.go
func login(opts loginOptions) error {
	if opts.passwordStdin {
		contents, err := ioutil.ReadAll(os.Stdin)
		if err != nil {
			return err
		}

		opts.password = strings.TrimSuffix(string(contents), "\n")
		opts.password = strings.TrimSuffix(opts.password, "\r")
	}
	if opts.user == "" && opts.password == "" {
		return errors.New("username and password required")
	}
	cf, err := config.Load(os.Getenv("DOCKER_CONFIG"))
	if err != nil {
		return err
	}
	creds := cf.GetCredentialsStore(opts.serverAddress)
	if opts.serverAddress == name.DefaultRegistry {
		opts.serverAddress = authn.DefaultAuthKey
	}
	if err := creds.Store(types.AuthConfig{
		ServerAddress: opts.serverAddress,
		Username:      opts.user,
		Password:      opts.password,
	}); err != nil {
		return err
	}

	if err := cf.Save(); err != nil {
		return err
	}
	log.Printf("logged in via %s", cf.Filename)
	return nil
}
