package main

// We use the external package cobra from spf13, the harbor-cli commands is
// custom package made by Logius

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/harbor-cli/commands"
)

// the Cobra package delivers the function rootCmd to start a cli command
// The funtion rootCmd.Execute starts the command from the commands package.

func main() {
	rootCmd := &cobra.Command{}
	//We have to do this otherwise we get the Usage printed at every error which is not helpful.
	rootCmd.SilenceUsage = true
	// We already print the error, we don't need a secondary print
	rootCmd.SilenceErrors = true
	commands.AddSubCommands(rootCmd)

	// the Execute function runs the cmd line
	if err := rootCmd.Execute(); err != nil {
		println(err.Error())
		os.Exit(1)
	}
}
