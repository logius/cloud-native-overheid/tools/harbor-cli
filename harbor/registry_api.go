package harbor

import (
	"fmt"
	"log"

	"gitlab.com/logius/cloud-native-overheid/tools/harbor-cli/config"
)

// Registry defines a registry in Harbor
type Registry struct {
	Name string `json:"name"`
	ID   int    `json:"id"`
}

// Look if registry exists by registryname returns registry.ID
// Returns 0 if registry not found
func (harborClient Client) GetRegistryID(registryName string) (int, error) {
	log.Printf("registryName: %s", registryName)
	harborRegistries := make([]Registry, 0)
	err := harborClient.doGetRequest(fmt.Sprintf("registries?name=%s", registryName), &harborRegistries)
	if err != nil {
		return -1, err
	}
	log.Printf("harborRegistries: %v", harborRegistries)
	for _, registry := range harborRegistries {
		log.Printf("registry name: %s registryID: %v", registry.Name, registry.ID)
		if registry.Name == registryName {
			log.Printf("Found existing registry name: %s registryID: %v", registry.Name, registry.ID)
			return registry.ID, nil
		}
	}
	return 0, nil
}

// EnsureRegistryExists creates a registry in Harbor
func (harborClient Client) EnsureRegistryExists(registry config.Registry) (RegistryID int, err error) {
	log.Printf("Ensure Harbor registry %s exists\n", registry.Name)
	registryID, err := harborClient.GetRegistryID(registry.Name)
	if err != nil {
		return -1, err
	}
	if registryID == 0 {
		type NewRegistry struct {
			Name string `json:"name"`
			Type string `json:"type"`
			Url  string `json:"url"`
		}

		newRegistry := &NewRegistry{
			Name: registry.Name,
			Type: registry.Type,
			Url:  registry.Url,
		}
		log.Printf("newRegistry: %s\n", newRegistry)
		harborClient.doPostRequest("registries", newRegistry, nil)
	}
	// lookup after adding new registry
	registryID, err = harborClient.GetRegistryID(registry.Name)
	if err != nil {
		return -1, err
	}
	if registryID < 0 {
		return -1, fmt.Errorf("could not find Harbor registry %s", registry.Name)
	}

	return registryID, nil
}
