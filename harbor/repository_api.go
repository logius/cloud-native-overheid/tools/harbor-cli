package harbor

import (
	"fmt"
	"strings"
)

type Repository struct {
	Name string
}

type Tag struct {
	Name string
}

type Artifact struct {
	Name string
	Tags []Tag
}

func (harborClient Client) GetRepositories(projectName string) ([]Repository, error) {

	repositories := make([]Repository, 0)
	for pageNr := 1; true; pageNr++ {

		pageRepositories := make([]Repository, 0)
		err := harborClient.doGetRequest(fmt.Sprintf("projects/%s/repositories?page_size=50&page=%d", projectName, pageNr), &pageRepositories)
		if err != nil {
			return nil, err
		}
		repositories = append(repositories, pageRepositories...)

		if len(pageRepositories) < 50 {
			break
		}
	}
	return repositories, nil
}

func (harborClient Client) GetArtifacts(projectName string, repositoryName string) ([]Artifact, error) {

	repositoryName = strings.Replace(repositoryName, projectName+"/", "", 1)
	repositoryName = strings.ReplaceAll(repositoryName, "/", "%252F")

	artifacts := make([]Artifact, 0)
	for pageNr := 1; true; pageNr++ {

		pageArtifacts := make([]Artifact, 0)
		err := harborClient.doGetRequest(fmt.Sprintf("projects/%s/repositories/%s/artifacts?page_size=50&page=%d", projectName, repositoryName, pageNr), &pageArtifacts)
		if err != nil {
			return nil, err
		}
		artifacts = append(artifacts, pageArtifacts...)
		if len(pageArtifacts) < 50 {
			break
		}
	}
	return artifacts, nil
}
