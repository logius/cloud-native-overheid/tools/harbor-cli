package harbor

import (
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
)

func CreateSuffixedGitlabGroupVars(groupVars map[string]gitlabclient.GroupVariable, suffixes []string) map[string]gitlabclient.GroupVariable {

	if suffixes == nil {
		return groupVars
	}
	groupVarsSuffixed := make(map[string]gitlabclient.GroupVariable)
	for key, value := range groupVars {
		for _, suffix := range suffixes {
			groupVarsSuffixed[key+suffix] = value
		}
	}
	return groupVarsSuffixed
}
