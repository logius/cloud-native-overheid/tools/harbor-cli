package harbor

import (
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"strings"

	"github.com/hashicorp/go-version"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
)

// Access specifies permissions for the RobotAccount
type Access struct {
	Action   string `json:"action"`
	Resource string `json:"resource"`
}

// Permission defines a permission for the RobotAccount
type Permission struct {
	Namespace string   `json:"namespace"`
	Kind      string   `json:"kind"`
	Access    []Access `json:"access"`
}

// RobotAccount represents a RobotAccount in a Harbor project
type RobotAccount struct {
	ID          int          `json:"id,omitempty"`
	Name        string       `json:"name"`
	Description string       `json:"description"`
	Level       string       `json:"level"`
	Permissions []Permission `json:"permissions,omitempty"`
	Access      []Access     `json:"access,omitempty"`
	Duration    int          `json:"duration"`
	Expires     int          `json:"expires_at,omitempty"`
}

// RobotCredentials has the username and token of the robot account; 'token' changed to 'secret' in 2.2.0.
type RobotCredentials struct {
	Name   string `json:"name"`
	Token  string `json:"token"`
	Secret string `json:"secret"`
}

type ConfigurationItem struct {
	Value    interface{} `json:"value"`
	Editable bool        `json:"editable"`
}

// SystemInfo represents Harbors system info
type SystemInfo struct {
	AuthMode                    string `json:"auth_mode"`
	ExternalUrl                 string `json:"external_url"`
	HarborVersion               string `json:"harbor_version"`
	HasCaRoot                   bool   `json:"has_ca_root"`
	NotificationEnable          bool   `json:"notification_enable"`
	ProjectCreationRestriction  string `json:"project_creation_restriction"`
	ReadOnly                    bool   `json:"read_only"`
	RegistryStorageProviderName string `json:"registry_storage_provider_name"`
	RegistryUrl                 string `json:"registry_url"`
	SelfRegistration            bool   `json:"self_registration"`
	WithChartmuseum             bool   `json:"with_chartmuseum"`
	WithNotary                  bool   `json:"with_notary"`
}

var configurations = make(map[string]ConfigurationItem)

var systemInfo SystemInfo

var robotNamePrefix string

// ManageProjectRobotAccounts creates new robot accounts on project level
func (harborClient Client) ManageProjectRobotAccounts(projectID int, harborProjectName string, defaultProject bool, createDockerAuthConfig bool) (map[string]gitlabclient.GroupVariable, error) {
	prefix, err := harborClient.getRobotNamePrefix()
	if err != nil {
		return nil, err
	}

	err = harborClient.deleteExistingProjectRobotAccount(projectID, prefix, harborProjectName)
	if err != nil {
		return nil, err
	}
	robotCredentials, err := harborClient.createProjectRobotAccount(harborProjectName)
	if err != nil {
		return nil, err
	}

	const HarborRobotName = "HARBOR_ROBOT_NAME"
	const HarborRobotSecret = "HARBOR_ROBOT_SECRET"

	groupVars := make(map[string]gitlabclient.GroupVariable)

	encodedAccountName := strings.ReplaceAll(robotCredentials.Name, "$", "$$")
	var encodedRobotSecret, encodedUsernamePwd string
	if harborClient.isOldVersion() {
		encodedRobotSecret = b64.StdEncoding.EncodeToString([]byte(robotCredentials.Token))
		encodedUsernamePwd = b64.StdEncoding.EncodeToString([]byte(robotCredentials.Name + ":" + robotCredentials.Token))
	} else {
		encodedRobotSecret = b64.StdEncoding.EncodeToString([]byte(robotCredentials.Secret))
		encodedUsernamePwd = b64.StdEncoding.EncodeToString([]byte(robotCredentials.Name + ":" + robotCredentials.Secret))
	}

	// robot account specific for current Harbor project
	harborProjectNameUpper := strings.ToUpper(strings.ReplaceAll(harborProjectName, "-", "_"))
	groupVars[harborProjectNameUpper+"_"+HarborRobotName] = gitlabclient.GroupVariable{
		Value:  encodedAccountName,
		Masked: false,
	}
	groupVars[harborProjectNameUpper+"_"+HarborRobotSecret] = gitlabclient.GroupVariable{
		Value:  encodedRobotSecret,
		Masked: true,
	}

	// default robot account
	if defaultProject {
		groupVars[HarborRobotName] = gitlabclient.GroupVariable{
			Value:  encodedAccountName,
			Masked: false,
		}
		groupVars[HarborRobotSecret] = gitlabclient.GroupVariable{
			Value:  encodedRobotSecret,
			Masked: true,
		}
	}

	if createDockerAuthConfig {
		parsedUrl, err := url.Parse(harborClient.FQDN)
		if err != nil {
			return nil, err
		}
		fqdnFromHarborUrl := parsedUrl.Hostname()

		var encodedDockerAuthConfig string = `{"auths":{"` + fqdnFromHarborUrl + `":{"auth":"` + encodedUsernamePwd + `"}}}`
		groupVars["DOCKER_AUTH_CONFIG"] = gitlabclient.GroupVariable{
			Value:  encodedDockerAuthConfig,
			Masked: false, // cannot be masked because of certain characters in the value
		}
	}

	return groupVars, nil
}

func (harborClient Client) deleteExistingProjectRobotAccount(projectID int, robotNamePrefix string, harborProjectName string) error {
	robotAccounts := make([]RobotAccount, 0)
	err := harborClient.doGetRequest(fmt.Sprintf("projects/%d/robots", projectID), &robotAccounts)
	if err != nil {
		return err
	}
	var robotAccountName string

	if harborClient.isOldVersion() {
		robotAccountName = robotNamePrefix + "gitlab"
	} else {
		// project name is included in robot account username for versions >= 2.2
		robotAccountName = robotNamePrefix + harborProjectName + "+gitlab"
	}
	gitlabRobotAccount, found := findRobotAccount(robotAccounts, robotAccountName)

	if found {
		log.Printf("Remove existing robot account '%s' for project '%s' (id: %d)", robotAccountName, harborProjectName, projectID)
		err := harborClient.doDeleteRequest(fmt.Sprintf("projects/%d/robots/%d", projectID, gitlabRobotAccount.ID))
		if err != nil {
			return err
		}
	} else {
		log.Printf("Robot account '%s' for project '%s' not found", robotAccountName, harborProjectName)
	}
	return nil
}

func (harborClient Client) createProjectRobotAccount(projectName string) (*RobotCredentials, error) {

	name := "gitlab" // will be prefixed with 'robot$' (< v2.2) or '<robot_prefix_name><project_name>+' (>= v2.2)
	newRobotAccount := RobotAccount{
		Name:        name,
		Description: "Robot for GitLab",
		Duration:    -1,
		Expires:     -1,
		Level:       "project",
		Permissions: []Permission{{
			Kind:      "project",
			Namespace: projectName,
			Access: []Access{
				{
					Action:   "push",
					Resource: "repository",
				},
				{
					Action:   "pull",
					Resource: "repository",
				},
				{
					Action:   "read",
					Resource: "artifact",
				},
				{
					Action:   "delete",
					Resource: "artifact",
				},
				{
					Action:   "create",
					Resource: "tag",
				},
				{
					Action:   "delete",
					Resource: "tag",
				},
				{
					Action:   "create",
					Resource: "artifact-label",
				},
				{
					Action:   "create",
					Resource: "scan",
				},
			},
		},
		},
	}

	robotCredentials := RobotCredentials{}
	err := harborClient.doPostRequest("robots", &newRobotAccount, &robotCredentials)
	if err != nil {
		return nil, err
	}
	return &robotCredentials, nil
}

// ManageSystemRobotAccount creates a new robot account on system level
func (harborClient Client) ManageSystemRobotAccount(harborProjects map[string]int, customerName string, robotAccountLevel string, namePrefix string, dockerAuthConfigFile string, altHarborHostnames []string) (map[string]gitlabclient.GroupVariable, error) {

	prefix, err := harborClient.getRobotNamePrefix()
	if err != nil {
		return nil, err
	}

	err = harborClient.deleteExistingSystemRobotAccount(prefix, customerName)
	if err != nil {
		return nil, err
	}

	robotCredentials, err := harborClient.createSystemRobotAccount(harborProjects, customerName, robotAccountLevel)
	if err != nil {
		return nil, err
	}

	if namePrefix == "" {
		namePrefix = "HARBOR_"
	} else {
		if !strings.HasSuffix(namePrefix, "_") {
			namePrefix += "_"
		}
	}

	HarborRobotName := namePrefix + "ROBOT_NAME"
	HarborRobotSecret := namePrefix + "ROBOT_SECRET"

	groupVars := make(map[string]gitlabclient.GroupVariable)

	encodedAccountName := strings.ReplaceAll(robotCredentials.Name, "$", "$$")
	var encodedRobotSecret, encodedUsernamePwd string
	encodedRobotSecret = b64.StdEncoding.EncodeToString([]byte(robotCredentials.Secret))
	encodedUsernamePwd = b64.StdEncoding.EncodeToString([]byte(robotCredentials.Name + ":" + robotCredentials.Secret))

	groupVars[HarborRobotName] = gitlabclient.GroupVariable{
		Value:  encodedAccountName,
		Masked: false,
	}
	groupVars[HarborRobotSecret] = gitlabclient.GroupVariable{
		Value:  encodedRobotSecret,
		Masked: true,
	}

	parsedUrl, err := url.Parse(harborClient.FQDN)
	if err != nil {
		return nil, err
	}
	fqdnFromHarborUrl := parsedUrl.Hostname()

	dockerAuthConfigJson, err := harborClient.generateDockerAuthConfig(fqdnFromHarborUrl, encodedUsernamePwd, dockerAuthConfigFile, altHarborHostnames)
	if err != nil {
		return nil, err
	}

	groupVars["DOCKER_AUTH_CONFIG"] = gitlabclient.GroupVariable{
		Value:  dockerAuthConfigJson,
		Masked: false, // cannot be masked because of certain characters in the value
	}

	return groupVars, nil
}

// The DockerAuthConfig contains "auths", which is a map of {auth:"password"} in JSON, named "DockerAuth"
// var EmptyAuth = []byte(`{"auths":{"harbor1.nl":{"auth":"pass"},"harbor2.nl":{"auth":"pass"},"harbor3.nl":{"auth":"pass"}}}`)
type DockerAuth struct {
	Password string `json:"auth"`
}
type DockerAuthConfig struct {
	Auths map[string]DockerAuth `json:"auths"`
}

// var mydockerauth DockerAuthConfig
// err:= json.Unmarshal(EmptyAuth, &mydockerauth)

func (harborClient Client) generateDockerAuthConfig(fqdn string, pass string, dockerAuthConfigFile string, altHarborUrls []string) (string, error) {
	var generatedDockerAuthConfig DockerAuthConfig = DockerAuthConfig{Auths: make(map[string]DockerAuth)}

	existingConfig, err := ioutil.ReadFile(dockerAuthConfigFile)
	if err == nil {
		err := json.Unmarshal(existingConfig, &generatedDockerAuthConfig)
		if err != nil {
			fmt.Println("Could not read dockerAuthConfig json " + dockerAuthConfigFile)
			return "", err
		}
	}

	// Add existing configs(DockerAuth)
	newAuth := DockerAuth{Password: pass}
	generatedDockerAuthConfig.Auths[fqdn] = newAuth

	for _, altHarborUrl := range altHarborUrls {
		parsedUrl, err := url.Parse(altHarborUrl)
		if err != nil {
			return "", err
		}
		hostname := parsedUrl.Hostname()
		generatedDockerAuthConfig.Auths[hostname] = newAuth
	}

	answer, _ := json.MarshalIndent(generatedDockerAuthConfig, "", "  ")
	err = ioutil.WriteFile(dockerAuthConfigFile, answer, 0644)
	if err != nil {
		return "", err
	}

	return string(answer[:]), nil
}

func (harborClient Client) deleteExistingSystemRobotAccount(robotNamePrefix string, customerName string) error {
	robotAccounts := make([]RobotAccount, 0)
	err := harborClient.doGetRequest("robots?page_size=-1", &robotAccounts)
	if err != nil {
		return err
	}

	var robotAccountName = robotNamePrefix + customerName
	RobotAccount, found := findRobotAccount(robotAccounts, robotAccountName)

	if found {
		log.Printf("Remove existing System Robot account '%s' for customer '%s'", robotAccountName, customerName)
		err := harborClient.doDeleteRequest(fmt.Sprintf("robots/%d", RobotAccount.ID))
		if err != nil {
			return err
		}
	} else {
		log.Printf("System Robot account '%s' for customer '%s' not found", robotAccountName, customerName)
	}
	return nil
}

func (harborClient Client) createSystemRobotAccount(harborProjects map[string]int, customerName string, robotAccountLevel string) (*RobotCredentials, error) {
	permissions := buildSystemRobotPermissions(harborProjects)
	level := robotAccountLevel
	newRobotAccount := RobotAccount{
		Name:        customerName,
		Level:       level,
		Description: "System Robot Account for: " + customerName,
		Duration:    -1,
		Permissions: permissions,
	}

	robotCredentials := RobotCredentials{}
	err := harborClient.doPostRequest("robots", &newRobotAccount, &robotCredentials)
	if err != nil {
		return nil, err
	}
	return &robotCredentials, nil
}

// Build the System Robot permissions list
func buildSystemRobotPermissions(harborProjects map[string]int) (permissions []Permission) {
	for harborProject := range harborProjects {
		permission := Permission{
			Kind:      "project",
			Namespace: harborProject,
			Access: []Access{
				{
					Action:   "list",
					Resource: "artifact",
				},
				{
					Action:   "list",
					Resource: "label",
				},
				{
					Action:   "list",
					Resource: "repository",
				},
				{
					Action:   "list",
					Resource: "tag",
				},
				{
					Action:   "list",
					Resource: "tag-retention",
				},
				{
					Action:   "pull",
					Resource: "repository",
				},
				{
					Action:   "push",
					Resource: "repository",
				},
				{
					Action:   "create",
					Resource: "artifact-label",
				},
				{
					Action:   "create",
					Resource: "scan",
				},
				{
					Action:   "create",
					Resource: "tag",
				},
				{
					Action:   "delete",
					Resource: "artifact",
				},
				{
					Action:   "delete",
					Resource: "tag",
				},
				{
					Action:   "read",
					Resource: "artifact",
				},
				{
					Action:   "read",
					Resource: "label",
				},
				{
					Action:   "read",
					Resource: "project",
				},
				{
					Action:   "read",
					Resource: "repository",
				},
				{
					Action:   "read",
					Resource: "scan",
				},
				{
					Action:   "read",
					Resource: "tag-retention",
				},
				{
					Action:   "read",
					Resource: "artifact-addition",
				},
			},
		}
		permissions = append(permissions, permission)
	}
	return permissions
}

func findRobotAccount(robotAccounts []RobotAccount, accountName string) (robotAccount RobotAccount, found bool) {
	for _, robotAccount := range robotAccounts {
		if robotAccount.Name == accountName {
			return robotAccount, true
		}
	}
	return
}

// fetch configurations from Harbor and keep result in var configurations
func (harborClient Client) fetchConfigurations() error {
	if len(configurations) == 0 {
		if err := harborClient.doGetRequest("configurations", &configurations); err != nil {
			return err
		}
	}
	return nil
}

// get robot name prefix setting from configurations
func (harborClient Client) getRobotNamePrefix() (string, error) {

	if robotNamePrefix == "" {
		if err := harborClient.fetchConfigurations(); err != nil {
			return "", err
		}

		robotNamePrefix = "robot$" // this is the default
		for name, configurationItem := range configurations {
			if name == "robot_name_prefix" {
				robotNamePrefix = configurationItem.Value.(string)
				log.Printf("Configuration item '%s': value: %v, editable: %v", name, configurationItem.Value, configurationItem.Editable)
			}
		}
	}
	return robotNamePrefix, nil
}

// fetch system info from Harbor and keep result in var systemInfo
func (harborClient Client) fetchSystemInfo() error {
	if systemInfo.HarborVersion == "" {
		if err := harborClient.doGetRequest("systeminfo", &systemInfo); err != nil {
			return err
		}
	}
	return nil
}

func (harborClient Client) isOldVersion() bool {
	if err := harborClient.fetchSystemInfo(); err != nil {
		log.Printf("Assuming using a new version; issue getting system info: %+v", err)
		return false
	}
	currentVersion, _ := version.NewVersion(systemInfo.HarborVersion)
	minVersion, _ := version.NewVersion("v2.2")
	return currentVersion.LessThan(minVersion)
}
