package harbor

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// Http Methods
const (
	MethodGet    = "GET"
	MethodPost   = "POST"
	MethodPut    = "PUT"
	MethodDelete = "DELETE"
)

// Content types
const (
	jsonContentType = "application/json"
)

// Client struct with api methods and accesstoken
type Client struct {
	FQDN     string // contains harborUrl; TODO change name
	Username string
	Password string
}

func (harborClient Client) doGetRequest(requestPath string, response interface{}) error {
	return harborClient.doJSONRequest(MethodGet, requestPath, nil, response, 200)
}

func (harborClient Client) doDeleteRequest(requestPath string) error {
	return harborClient.doJSONRequest(MethodDelete, requestPath, nil, nil, 200)
}

func (harborClient Client) doPutRequest(requestPath string, body interface{}) error {
	return harborClient.doJSONRequest(MethodPut, requestPath, body, nil, 200)
}

func (harborClient Client) doPostRequest(requestPath string, body interface{}, response interface{}) error {
	return harborClient.doJSONRequest(MethodPost, requestPath, body, response, 201)
}

func (harborClient Client) doJSONRequest(method string, requestPath string, body interface{}, response interface{}, allowedStatuscode int) error {

	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(body)

	url := fmt.Sprintf("%s/api/v2.0/%s", harborClient.FQDN, requestPath)
	req, err := http.NewRequest(method, url, buf)
	if err != nil {
		return fmt.Errorf("error while creating a new http request for \"%s\": %v", url, err)
	}
	req.SetBasicAuth(harborClient.Username, harborClient.Password)
	req.Header.Set("Content-Type", jsonContentType)

	var client = &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("error while performing http request: %v", err)
	}
	defer res.Body.Close()

	if res.StatusCode != allowedStatuscode {
		log.Println("unexpected status code - method ", method, " ", requestPath, "Statuscode ", res.StatusCode)
		json, _ := json.Marshal(body)
		// responseData = json response message
		responseData, _ := ioutil.ReadAll(res.Body)
		return fmt.Errorf("URL: %s, Method %s, Body: %s, Error status code: %d responseData: %v", url, method, json, res.StatusCode, string(responseData))
	}

	responseData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("unable to perform action readAll: %v", err)
	}

	// fmt.Printf("URL: %s, Method %s, Body: %s, Response: %s\n", url, method, body, responseData)

	if len(responseData) > 0 {
		err = json.Unmarshal(responseData, response)
		if err != nil {
			return fmt.Errorf("URL: %s, Method %s, Body: %s, Response: %s, error: %s", url, method, body, responseData, err)
		}
	}

	return nil
}
