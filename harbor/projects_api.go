package harbor

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"strconv"
	"strings"

	"code.cloudfoundry.org/bytefmt"
	"gitlab.com/logius/cloud-native-overheid/tools/harbor-cli/config"
)

// Project defines a project in Harbor
type Project struct {
	Name string `json:"name"`
	ID   int    `json:"project_id"`
}

// UserGroup defines a usergroup in Harbor
type UserGroup struct {
	Name string `json:"group_name"`
	ID   int    `json:"id"`
	Type int    `json:"group_type"`
}

// Member defines a member in a Harbor project
type Member struct {
	ID     int       `json:"id"`
	RoleID int       `json:"role_id"`
	Group  UserGroup `json:"member_group"`
}

// ExistingMember represents an existing member in Harbor
type ExistingMember struct {
	ID         int    `json:"id"`
	EntityName string `json:"entity_name"`
	EntityType string `json:"entity_type"`
}

type RID struct {
	Retention_id string `json:"retention_id"`
}

// GetProjects fetches all projects from Harbor
func (harborClient Client) GetProjects() ([]Project, error) {
	log.Println("Get all projects")
	harborProjects := make([]Project, 0)
	err := harborClient.doGetRequest("projects?page_size=-1", &harborProjects)
	if err != nil {
		return nil, err
	}
	return harborProjects, nil
}

// Look if project exists by projectname returns project.ID
// Returns -1 if project not found
func (harborClient Client) GetProjectID(projectName string) (int, error) {
	harborProjects := make([]Project, 0)
	err := harborClient.doGetRequest(fmt.Sprintf("projects?name=%s", projectName), &harborProjects)
	if err != nil {
		return -1, err
	}
	for _, project := range harborProjects {
		if project.Name == projectName {
			log.Printf("Found existing project name: %s projectID: %v\n", project.Name, project.ID)
			return project.ID, nil
		}
	}
	return -1, nil
}

// EnsureProjectExists creates or updates a project in Harbor
func (harborClient Client) EnsureProjectExists(project config.Project, overwriteRetentionPolicy bool ) (ProjectID int, err error) {
	log.Printf("Ensure Harbor project %s exists\n", project.Name)

	// ProjectMetaData defines the metadata for a project in Harbor
	type ProjectMetaData struct {
		PreventVulnerableImagesFromRunning         string `json:"prevent_vul"`
		PreventVulnerableImagesFromRunningSeverity string `json:"severity"`
		AutomaticallyScanImagesOnPush              string `json:"auto_scan"`
		Public                                     string `json:"public"`
	}

	projectMetaData := ProjectMetaData{
		PreventVulnerableImagesFromRunning:         project.PreventingVulnFromRunning(),
		PreventVulnerableImagesFromRunningSeverity: "high",
		AutomaticallyScanImagesOnPush:              "true",
		Public:                                     strconv.FormatBool(project.IsPublic()),
	}

	type HarborProjectWithMetaData struct {
		Name     string          `json:"project_name"`
		MetaData ProjectMetaData `json:"metadata"`
		Registry int             `json:"registry_id,omitempty"`
	}

	type HarborProject struct {
		Name     string `json:"project_name"`
		Registry int    `json:"registry_id,omitempty"`
	}

	projectId, err := harborClient.GetProjectID(project.Name)
	if err != nil {
		return -1, err
	}

	registryID := 0

	// Find Registry if one is defined in the config
	if project.RegistryName != "" {
		registryID, err = harborClient.GetRegistryID(project.RegistryName)
		if err != nil {
			return -1, err
		}
	}

	isNewProject := false

	// project does not exist, create it
	if projectId == -1 {

		newProject := &HarborProjectWithMetaData{
			Name:     project.Name,
			MetaData: projectMetaData,
			Registry: registryID,
		}
		newProject.MetaData = projectMetaData
		log.Printf("create project: %+v", newProject)
		if err = harborClient.doPostRequest("projects", newProject, nil); err != nil {
			return -1, err
		}

		isNewProject = true

		// lookup after adding new project
		if projectId, err = harborClient.GetProjectID(project.Name); err != nil {
			log.Printf("Project not found after create project: %+v", err)
			return -1, err
		}
	} else {
		// no metadata changes on existing projects (leave that to the projects admins)
		existingProject := &HarborProject{
			Name:     project.Name,
			Registry: registryID,
		}
		log.Printf("Update project: id:%d, %+v", projectId, existingProject)
		if err = harborClient.doPutRequest(fmt.Sprintf("projects/%v", projectId), existingProject); err != nil {
			return -1, err
		}

	}

	if projectId < 0 {
		return -1, fmt.Errorf("could not find Harbor project %s", project.Name)
	}

	// only for new projects OR when --overwrite-retention-policy has been issued, create or overwrite retention policies if config contains a retention-policy file
	if project.RetentionFile != "" && ( isNewProject || overwriteRetentionPolicy ) {
		log.Printf("apply retention file: %+v", project.RetentionFile)
		if err = harborClient.EnsureProjectRetentionsExists(project, projectId); err != nil {
			return -1, err
		}
	}

	if err = harborClient.ManageQuota(project, projectId); err != nil {
		return -1, err
	}

	if err = harborClient.EnsureUserGroupsExists(project, projectId); err != nil {
		return -1, err
	}
	if err = harborClient.EnsureProjectMembersExists(project, projectId); err != nil {
		return -1, err
	}
	if err = harborClient.RemoveRedundantProjectMembers(project, projectId); err != nil {
		return -1, err
	}
	return projectId, nil
}

// EnsureUserGroupsExists creates a new user group if needed
func (harborClient Client) EnsureUserGroupsExists(project config.Project, projectID int) error {

	for _, rolebinding := range project.Rolebindings {
		for _, groupName := range rolebinding.Groups {
			log.Printf("Ensure usergroup %s exists in Harbor", groupName)

			userGroups := make([]UserGroup, 0)
			harborClient.doGetRequest("usergroups/search?groupname="+groupName, &userGroups)
			needsToBeCreated := true
			if len(userGroups) != 0 {
				for _, group := range userGroups {
					if group.Name == groupName {
						log.Printf("Group %s exists in Harbor")
						needsToBeCreated = false
					}
				}
			}

			if needsToBeCreated {
				newGroup := &UserGroup{
					Name: groupName,
					Type: 3,
				}
				err := harborClient.doPostRequest("usergroups", newGroup, nil)
				if err != nil {
					return err
				}
				userGroups = append(userGroups, *newGroup)
				log.Printf("Group %s created")
			}
		}
	}
	return nil
}

// EnsureProjectMembersExists creates members in the project if needed
func (harborClient Client) EnsureProjectMembersExists(project config.Project, projectID int) error {

	for _, rolebinding := range project.Rolebindings {
		log.Printf("Ensure role %s exists in Harbor project %s", rolebinding.Name, project.Name)

		for _, groupName := range rolebinding.Groups {

			log.Printf("Ensure usergroup %s for role %s exists in Harbor project %s", groupName, rolebinding.Name, project.Name)

			userGroups := make([]UserGroup, 0)
			harborClient.doGetRequest("usergroups/search?groupname="+groupName, &userGroups)

			if len(userGroups) == 0 {
				return fmt.Errorf("could not find usergroup %s", groupName)
			}
			members := make([]Member, 0)
			err := harborClient.doGetRequest(fmt.Sprintf("projects/%d/members?entityname=%s", projectID, groupName), &members)
			if err != nil {
				return err
			}

			roleID, err := roleNameToHarborID(rolebinding.Name)
			if err != nil {
				return err
			}
			if len(members) > 0 {
				// update member if the role was changed
				if members[0].RoleID != roleID {
					updateMember := &Member{RoleID: roleID}
					err := harborClient.doPutRequest(fmt.Sprintf("projects/%d/members/%d", projectID, members[0].ID), updateMember)
					if err != nil {
						return err
					}
				}
			} else {
				// create new member
				roleID, err := roleNameToHarborID(rolebinding.Name)
				if err != nil {
					return err
				}

				newMember := &Member{
					RoleID: roleID,
					Group: UserGroup{
						ID:   userGroups[0].ID,
						Name: groupName,
					},
				}

				err = harborClient.doPostRequest(fmt.Sprintf("projects/%d/members", projectID), newMember, nil)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

// RemoveRedundantProjectMembers removes members from project that do not exist in the project config anymore
func (harborClient Client) RemoveRedundantProjectMembers(project config.Project, projectID int) error {

	members := make([]ExistingMember, 0)
	err := harborClient.doGetRequest(fmt.Sprintf("projects/%d/members", projectID), &members)
	if err != nil {
		return err
	}
	// Remove actual groups from memberlist
	for _, rolebinding := range project.Rolebindings {
		for _, groupName := range rolebinding.Groups {
			members = removeGroupFromMembers(members, groupName)
		}
	}

	// Remove remaining items in memberlist
	for _, member := range members {
		if member.EntityType == "g" {
			log.Printf("Delete member %s from project %s\n", member.EntityName, project.Name)
			err := harborClient.doJSONRequest(MethodDelete, fmt.Sprintf("projects/%d/members/%d", projectID, member.ID), nil, nil, 200)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func removeGroupFromMembers(members []ExistingMember, groupName string) []ExistingMember {
	for index, member := range members {
		if member.EntityName == groupName {
			return append(members[:index], members[index+1:]...)
		}
	}
	return members
}

func roleNameToHarborID(role string) (id int, err error) {
	roles := map[string]int{
		"projectadmin": 1,
		"developer":    2,
		"guest":        3,
		"maintainer":   4,
		"limitedguest": 5,
	}

	id = roles[strings.ToLower(role)]
	if id == 0 {
		return -1, fmt.Errorf("the role %s is not supported", role)
	}
	return id, nil
}

// JSON Template
func parseDeploymentFile(fileName string, variables interface{}, object interface{}) {
	var output bytes.Buffer

	tpl := template.Must(template.ParseGlob(fileName))
	err := tpl.Execute(&output, variables)

	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(output.Bytes(), object)
	if err != nil {
		log.Fatal(err)
	}
}

func (harborClient Client) EnsureProjectRetentionsExists(project config.Project, projectID int) error {

	// get retention policy id
	pID := strconv.Itoa(projectID)
	var rid RID
	err := harborClient.doGetRequest("projects/"+pID+"/metadatas/retention_id", &rid)
	if err != nil {
		return err
	}

	// Used for JSON file template
	type retentionParams struct {
		ProjectId int
	}
	var httpRequest interface{}
	var httpResponse interface{}

	// Parse JSONFile
	parseDeploymentFile(project.RetentionFile, &retentionParams{projectID}, &httpRequest)

	// Add retention policy
	if rid.Retention_id != "" {
		err = harborClient.doPutRequest("retentions/"+rid.Retention_id, httpRequest)
	} else {
		err = harborClient.doPostRequest("retentions", httpRequest, httpResponse)
	}

	return err
}

func (harborClient Client) ManageQuota(project config.Project, projectID int) error {
	if project.StorageQuota == "" {
		return nil
	}
	type Quota struct {
		Hard map[string]uint64
	}
	storage, err := bytefmt.ToBytes(project.StorageQuota)
	if err != nil {
		return err
	}
	quota := &Quota{
		Hard: map[string]uint64{
			"storage": storage,
		},
	}

	log.Printf("Configure %s storage quota for Harbor project %s", project.StorageQuota, project.Name)
	return harborClient.doPutRequest(fmt.Sprintf("quotas/%d", projectID), quota)
}
